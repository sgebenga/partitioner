# README #
To run the application

$ mvn clean install
$ hadoop jar target/partitioner-1.0-SNAPSHOT.jar bd.partitioner.WordCount data/in/ data/out

### What is this repository for? ###

A simple Hadoop application with a custom Partitioner

### How do I get set up? ###

Requires Maven and Hadoop 2.0

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact